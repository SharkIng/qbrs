
<html>
<head>
  <title>Queens Bicycle Registration System</title>
</head>
<body>
<table cellspacing="50">
	<tr>
		<td>
			<img src="/pps/qbrs/images/Queens_logo.png" width="192" height="146">
		</td>
        <td align="center">
			<h1>Queens Bicycle Registration System</h1>
        </td>
		<td>
			<FORM METHOD="LINK" ACTION="https://webapp.queensu.ca/pps/qbrs/registered/index.php">
  			<INPUT TYPE="submit" VALUE="Login">
  			</FORM>
  		</td>
	</tr>
	<tr>
    	<td>
        	<!-- Blank Cell under the Queens Logo. -->
        </td>
    	<td align="center">
        	<h3>Enter the Serial Number Here to search for a Bicycle:</h3>
        	<input name="SerialSearch" type="text" id="SerialSearch" value="Enter Serial Number here to Search" size="50">
            <input name="SubmitSearch" type="button" value="Search">
        </td>
	</tr>
    <tr>
    	<td>
        	<!-- Blank Cell under the Queens Logo. -->
        </td>
        <td align="center">
        	<a href="http://youtu.be/JgHubY5Vw3Y" title="Video - How to properly lock your bicycle" target="new">Video - How to properly lock your bicycle</a>
            <br />
            <a href="http://www.cyclekingston.ca/" title="CYCLE Kingston" target="new">CYCLE Kingston</a>
            <br />
        </td>
</table>
</body>
</html>
